# jGridder
Effortlessly draw grids for maps and playing boards.

To **request features**, such as new grid shapes, feel free to 
open an issue on [GitLab](https://gitlab.com/brzrkr/jgridder) 
or submit a merge request with your contribution. 

## Creating a new file...
### From scratch
To draw a grid from scratch, you first need to set up a 
background. Do so by clicking the *New...* button.  
Doing so opens a dialog to select your desired background: 
pick the *width* and *height* of the file and a background 
colour, which, since all grids are saved in *.png* format, 
can be transparent.

After confirming your settings using *Create*, the main window 
will show a preview of the file. Obviously you won't be able to 
distinguish transparent backgrounds, but note that the initial 
prompt "*Open an image or create a new one...*" disappears, 
signifying that a file is currently open.

### From an existing image
You can very easily add a grid to an existing image, that will 
then be used as the background. Do so by clicking the 
*New from...* button.  
You will be prompted for an image file to open. Note that 
although most image formats are supported, the final result will 
be saved in *.png*.

## Adding a grid
Once a file is open, either from scratch or from an existing 
image, you will be able to click the *Add grid...* button.  
There is no limit on how many different grids can be added to 
the same file.

A dialog will open, asking for the following settings:  
- **Grid size**: the space to leave between each element of the 
grid. For example, if a square grid is drawn, *grid size* is the 
side of the square; for lines, it is the space between 
adjacent lines.
- **Line width**: the width of the lines that form the grid. You 
can draw bolder grids by increasing this number. Note that 
larger values will cover the free space between grid elements.
- **Colour**: the colour of the lines for this particular grid.
- **Padding**: the space to leave on each border around the grid.
- **Grid type**: the shape of the grid to draw, such as squares 
or vertical lines.

Once you have set all options, click *Confirm* to add the grid 
to the file and see a preview in the main window.

Click *Save as...* to save the current file to disk as a *.png* 
file. This format will retain good image quality and any 
transparency.
