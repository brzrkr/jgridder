module jGridder {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    
    opens com.gitlab.brzrkr.jgridder to javafx.fxml;
    exports com.gitlab.brzrkr.jgridder;
    exports com.gitlab.brzrkr.jgridder.grid;
}
