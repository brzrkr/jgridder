package com.gitlab.brzrkr.jgridder;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        String mainWindowLocation = "mainWindow.fxml";
        
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(mainWindowLocation));
        Parent root = loader.load();
        
        Scene scene = new Scene(root);
        
        MainWindowController controller = loader.getController();
        controller.setMainWindow(stage);

        stage.setTitle("jGridder");
        stage.setScene(scene);
        stage.setMinHeight(400.0);
        stage.setMinWidth(400.0);
        
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}