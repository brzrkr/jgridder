package com.gitlab.brzrkr.jgridder;

import com.gitlab.brzrkr.jgridder.grid.Grid;
import com.gitlab.brzrkr.jgridder.grid.GridShape;
import com.gitlab.brzrkr.jgridder.grid.GridStyle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


public class MainWindowController {
    private Stage mainWindow;
    
    private Grid currentGrid;
    
    @FXML
    protected Button saveAsButton;
    @FXML
    protected Button addGridButton;
    
    @FXML
    protected ImageView gridView;
    
    @FXML
    private Label imagePrompt;
    
    public void initialize() {}
    
    protected void updateImage() {
        final var image = currentGrid.asWritableImage();
        gridView.setImage(image);
        
        // Features that require a present grid can now be enabled.
        saveAsButton.setDisable(false);
        addGridButton.setDisable(false);
        imagePrompt.setOpacity(0.0);
    }
    
    void setMainWindow(final Stage mainWindow) { this.mainWindow = mainWindow; }
    
    @FXML
    protected void buttonClickedNewFile() {
        final var loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("newFileWindow.fxml"));
        final Parent root;
        try { root = loader.load(); }
        catch (IOException e) {
            IOErrorAlert();
            return;
        }
        
        final var scene = new Scene(root);
        
        final var newGridWindow = new Stage();
        newGridWindow.setScene(scene);
        newGridWindow.setResizable(false);
        newGridWindow.initModality(Modality.APPLICATION_MODAL);
        
        final NewFileWindowController controller = loader.getController();
        controller.setDialog(newGridWindow);
        
        newGridWindow.showAndWait();
        
        if (!controller.isOk())
            return;
        
        currentGrid = new Grid(
                controller.getWidthValue(),
                controller.getHeightValue(),
                controller.getBackgroundValue()
        );
        updateImage();
    }
    
    @FXML
    protected void buttonClickedNewFrom() {
        final var fileChooser = new FileChooser();
        
        // Build description of supported files.
        final String[] exts = ImageIO.getReaderFileSuffixes();
        final List<String> extPatterns = new LinkedList<>();
        for (String pattern : exts)
            extPatterns.add("*." + pattern);
        
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Image files", extPatterns)
        );
        
        final var file = fileChooser.showOpenDialog(mainWindow);
    
        // No file selected.
        if (file == null)
            return;
        
        BufferedImage baseImage;
        try { baseImage = ImageIO.read(file); }
        catch (IOException e) {
            final var alert = new Alert(Alert.AlertType.ERROR, "Could not read the file.");
            alert.showAndWait();
            return;
        }
        
        if (baseImage == null) {
            final var alert = new Alert(Alert.AlertType.ERROR, "Image format not supported.");
            alert.showAndWait();
            return;
        }
        
        currentGrid = new Grid(baseImage);
        updateImage();
    }
    
    @FXML
    protected void buttonClickedSaveAs() {
        // Avoid opening if no grid present.
        if (currentGrid == null)
            return;
        
        final var fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("PNG file", "*.png")
        );
        
        final var file = fileChooser.showSaveDialog(mainWindow);
        if (file == null)
            return;
        
        final var image = currentGrid.asBufferedImage();
        
        try { ImageIO.write(image, "png", file); }
        catch (IOException e) {
            final var alert = new Alert(Alert.AlertType.ERROR, "Could not save to file.");
            alert.showAndWait();
        }
    }
    
    @FXML
    protected void buttonClickedAddGrid() {
        // Avoid opening if no grid present.
        if (currentGrid == null)
            return;
        
        final var loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("addGridWindow.fxml"));
        final Parent root;
        try { root = loader.load(); }
        catch (IOException e) {
            IOErrorAlert();
            return;
        }
        
        final var scene = new Scene(root);
        
        final var addGridWindow = new Stage();
        addGridWindow.setScene(scene);
        addGridWindow.setResizable(false);
        addGridWindow.initModality(Modality.APPLICATION_MODAL);
        
        final AddGridWindowController controller = loader.getController();
        controller.setDialog(addGridWindow);
        
        addGridWindow.showAndWait();
        
        if (!controller.isOk())
            return;
    
        final var newStyle = new GridStyle(
                controller.getPaddingTopValue(),
                controller.getPaddingRightValue(),
                controller.getPaddingBottomValue(),
                controller.getPaddingLeftValue(),
                controller.getGridSizeValue(),
                controller.getLineWidthValue(),
                controller.getGridColourValue()
        );
        
        currentGrid.setStyle(newStyle);
        
        // Draw the requested grid on the image.
        drawGrid(controller.getGridShapeValue());
        
        updateImage();
    }
    
    private void drawGrid(final GridShape gridShapeValue) {
        switch (gridShapeValue) {
            case SQUARES:
                currentGrid.drawSquares();
                break;
            case LINES_VERTICAL:
                currentGrid.drawLines(Grid.Direction.VERTICAL);
                break;
            case LINES_HORIZONTAL:
                currentGrid.drawLines(Grid.Direction.HORIZONTAL);
                break;
            case HEXES_VERTICAL:
                currentGrid.drawHexes(Grid.Direction.VERTICAL);
                break;
            case HEXES_HORIZONTAL:
                currentGrid.drawHexes(Grid.Direction.HORIZONTAL);
                break;
        }
        
        updateImage();
    }
    
    @FXML
    protected void linkClickedAbout() {
        final var infoString = "https://gitlab.com/brzrkr/jGridder\n\n" +
                               "distributed under GNU GPLv3 or later\n" +
                               "see: https://spdx.org/licenses/GPL-3.0-or-later.html";
        final var headerString = "jGridder\n" +
                                 "\u00a9 2019 Federico Salerno\n";
        
        final var alert = new Alert(Alert.AlertType.INFORMATION, infoString);
        alert.setTitle("About jGridder");
        alert.setHeaderText(headerString);
        alert.showAndWait();
    }
    
    protected void IOErrorAlert() {
        final var errorString = "An error occurred.\n" +
                                       "Please let the author know (see About...)";
            final var alert = new Alert(Alert.AlertType.ERROR, errorString);
            alert.showAndWait();
    }
}