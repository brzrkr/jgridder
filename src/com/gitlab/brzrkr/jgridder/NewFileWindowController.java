package com.gitlab.brzrkr.jgridder;

import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class NewFileWindowController {
    private Stage dialog;
    
    /** True if the user confirmed. */
    private boolean ok = false;
    
    /** CSS pseudoclass for invalidly filled fields. */
    private final PseudoClass errorClass = PseudoClass.getPseudoClass("error");
    
    // Field values to pass to the mainWindow.
    private int widthValue;
    private int heightValue;
    private Color backgroundValue;
    
    @FXML
    protected TextField width;
    @FXML
    protected TextField height;
    
    @FXML
    protected ColorPicker background;
    
    public void initialize() {}
    
    @FXML
    protected void validate() {
        // Reset error visualisation on all fields.
        boolean error = false;
        width.pseudoClassStateChanged(errorClass, false);
        height.pseudoClassStateChanged(errorClass, false);
        
        // Ensure positive integer.
        final String widthContent = width.getText();
        if (!widthContent.chars().allMatch(Character::isDigit) || widthContent.isEmpty()) {
            width.pseudoClassStateChanged(errorClass, true);
            error = true;
        } else if (Integer.parseInt(widthContent) < 1) {
            width.pseudoClassStateChanged(errorClass, true);
            error = true;
        }
        
        final String heightContent = height.getText();
        if (!heightContent.chars().allMatch(Character::isDigit) || heightContent.isEmpty()) {
            height.pseudoClassStateChanged(errorClass, true);
            error = true;
        } else if (Integer.parseInt(heightContent) < 1) {
            height.pseudoClassStateChanged(errorClass, true);
            error = true;
        }
        
        // Check if validation passed.
        if (error)
            return;
        
        ok = true;
        
        widthValue = Integer.parseInt(widthContent);
        heightValue = Integer.parseInt(heightContent);
        backgroundValue = background.getValue();
        
        close();
    }
    
    @FXML
    protected void close() { dialog.close(); }
    
    void setDialog(final Stage dialog) { this.dialog = dialog; }
    
    public boolean isOk() { return ok; }
    
    public int getWidthValue() { return widthValue; }
    
    public int getHeightValue() { return heightValue; }
    
    public Color getBackgroundValue() { return backgroundValue; }
}