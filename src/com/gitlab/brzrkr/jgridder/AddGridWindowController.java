package com.gitlab.brzrkr.jgridder;

import com.gitlab.brzrkr.jgridder.grid.GridShape;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class AddGridWindowController {
    private Stage dialog;
    
    /** True if the user confirmed. */
    private boolean ok = false;
    
    /** CSS pseudoclass for invalidly filled fields. */
    private final PseudoClass errorClass = PseudoClass.getPseudoClass("error");
    
    // Fields to pass to MainWindow.
    private int paddingTopValue;
    private int paddingRightValue;
    private int paddingBottomValue;
    private int paddingLeftValue;
    private int gridSizeValue;
    private int lineWidthValue;
    private Color gridColourValue;
    private GridShape gridShapeValue;
    
    @FXML
    protected TextField paddingTop;
    @FXML
    protected TextField paddingRight;
    @FXML
    protected TextField paddingBottom;
    @FXML
    protected TextField paddingLeft;
    @FXML
    protected TextField gridSize;
    @FXML
    protected TextField lineWidth;
    
    @FXML
    protected ColorPicker gridColour;
    
    @FXML
    protected ChoiceBox<GridShape> gridShape;
    
    public void initialize() {
        // Initialise ChoiceBox items.
        for (GridShape shape : GridShape.values())
            gridShape.getItems().add(shape);
        gridShape.getSelectionModel().select(0);
    }
    
    @FXML
    protected void validate() {
        // Reset error visualisation on all fields.
        boolean valid = false;
        paddingTop.pseudoClassStateChanged(errorClass, false);
        paddingRight.pseudoClassStateChanged(errorClass, false);
        paddingBottom.pseudoClassStateChanged(errorClass, false);
        paddingLeft.pseudoClassStateChanged(errorClass, false);
        gridSize.pseudoClassStateChanged(errorClass, false);
        lineWidth.pseudoClassStateChanged(errorClass, false);
        
        if (
                isValidInteger(paddingTop) && isValidInteger(paddingRight) &&
                isValidInteger(paddingBottom) && isValidInteger(paddingLeft) &&
                isValidPositiveInteger(gridSize) && isValidPositiveInteger(lineWidth)
        )
            valid = true;
        
        // Check if validation passed.
        if (!valid)
            return;
        
        ok = true;
        
        paddingTopValue = Integer.parseInt(paddingTop.getText());
        paddingRightValue = Integer.parseInt(paddingRight.getText());
        paddingBottomValue = Integer.parseInt(paddingBottom.getText());
        paddingLeftValue = Integer.parseInt(paddingLeft.getText());
        gridSizeValue = Integer.parseInt(gridSize.getText());
        lineWidthValue = Integer.parseInt(lineWidth.getText());
        
        gridColourValue = gridColour.getValue();
        
        gridShapeValue = gridShape.getValue();
        
        close();
    }
    
    @FXML
    protected void close() { dialog.close(); }
    
    void setDialog(final Stage dialog) { this.dialog = dialog; }
    
    public boolean isOk() { return ok; }
    
    public int getPaddingTopValue() { return paddingTopValue; }
    
    public int getPaddingRightValue() { return paddingRightValue; }
    
    public int getPaddingBottomValue() { return paddingBottomValue; }
    
    public int getPaddingLeftValue() { return paddingLeftValue; }
    
    public int getGridSizeValue() { return gridSizeValue; }
    
    public int getLineWidthValue() { return lineWidthValue; }
    
    public Color getGridColourValue() { return gridColourValue; }
    
    public GridShape getGridShapeValue() { return gridShapeValue; }
    
    /**
     * Check whether the content of the given field is a positive integer. Mark it with
     * the error pseudoclass if not.
     *
     * @param field the TextField to validate.
     *
     * @return true if and only if the field's value is a positive integer.
     */
    private boolean isValidPositiveInteger(TextField field) {
        boolean result = true;
        final String fieldContent = field.getText();
        
        if (!isValidInteger(field)) {
            result = false;
        } else if (Integer.parseInt(fieldContent) < 1) {
            field.pseudoClassStateChanged(errorClass, true);
            result = false;
        }
        
        return result;
    }
    
    private boolean isValidInteger(TextField field) {
        boolean result = true;
        final String fieldContent = field.getText();
        
        if (fieldContent.isEmpty() || !fieldContent.chars().allMatch(Character::isDigit)) {
            field.pseudoClassStateChanged(errorClass, true);
            result = false;
        }
        
        return result;
    }
}