package com.gitlab.brzrkr.jgridder.grid;

import java.awt.*;


public class ColourConverter {
    public static Color javaFXToAWT(javafx.scene.paint.Color javaFXColour) {
        return new Color(
                (float)javaFXColour.getRed(),
                (float)javaFXColour.getGreen(),
                (float)javaFXColour.getBlue(),
                (float)javaFXColour.getOpacity()
        );
    }
}
