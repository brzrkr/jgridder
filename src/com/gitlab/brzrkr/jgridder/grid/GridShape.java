package com.gitlab.brzrkr.jgridder.grid;

public enum GridShape {
    SQUARES("Squares"),
    LINES_VERTICAL("Vertical lines"),
    LINES_HORIZONTAL("Horizontal lines"),
    HEXES_VERTICAL("Vertical hexagons"),
    HEXES_HORIZONTAL("Horizontal hexagons");
    
    private final String name;
    
    GridShape(final String name) { this.name = name; }
    
    @Override
    public String toString() { return name; }
}
