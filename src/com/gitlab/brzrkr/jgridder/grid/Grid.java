package com.gitlab.brzrkr.jgridder.grid;

import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;


public class Grid {
    private final BufferedImage image;
    private GridStyle style;
    public enum Direction {
        HORIZONTAL,
        VERTICAL
    }
    
    private static class ValueError extends Error {}
    
    /**
     * Create a Grid starting from an existing image.
     *
     * @param image existing image to use as base.
     */
    public Grid(final Image image, final GridStyle style) {
        // Convert image to correct type.
        this.image = new BufferedImage(
                image.getWidth(null),
                image.getHeight(null),
                BufferedImage.TYPE_INT_ARGB
        );
        
        // Copy contents to new image.
        final Graphics2D graphics = this.image.createGraphics();
        graphics.drawImage(image, 0, 0, null);
        graphics.dispose();
        
        setStyle(style);
    }
    
    public Grid(final Image image) {
        this(image, new GridStyle());
    }
    
    /**
     * Create an empty Grid.
     *
     * @param width width of the canvas.
     * @param height height of the canvas.
     */
    public Grid(final int width, final int height, final GridStyle style, final Color background) {
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        
        // Fill image with background colour.
        Graphics2D graphics = image.createGraphics();
        
        java.awt.Color convertedBgColour = ColourConverter.javaFXToAWT(background);
        graphics.setColor(convertedBgColour);
        graphics.fillRect(0, 0, image.getWidth() - 1, image.getHeight() - 1);
        
        graphics.dispose();
        
        setStyle(style);
    }
    
    public Grid(final int width, final int height, Color background) {
        this(width, height, new GridStyle(), background);
    }
    
    public void setStyle(final GridStyle style) {
        // Ensure padding does not exceed image size.
        if (style.getPaddingTop() + style.getPaddingBottom() > image.getHeight()
                || style.getPaddingLeft() + style.getPaddingRight() > image.getWidth())
            throw new ValueError();
        
        this.style = style;
    }
    
    /** @return WritableImage of the grid, suitable for use with JavaFX. */
    public WritableImage asWritableImage() {
        final int width = image.getWidth();
        final int height = image.getHeight();
        final var writableImage = new WritableImage(width, height);
        final var pixelWriter = writableImage.getPixelWriter();
        
        // Copy over each pixel.
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                pixelWriter.setArgb(x, y, image.getRGB(x, y));
 
        return writableImage;
    }
    
    public BufferedImage asBufferedImage() { return image; }
    
    public void drawLines(final Direction direction) {
        final Graphics2D graphics = image.createGraphics();
        graphics.setColor(style.getGridColour());
        graphics.setStroke(new BasicStroke(style.getLineWidth()));
        
        switch (direction) {
            case VERTICAL:
                for (int i = 0; i < image.getWidth(); i += style.getGridSize()) {
                    final double x = Math.min(
                            Math.max(i, style.getPaddingLeft()),
                            image.getWidth() - style.getPaddingRight()
                    );
        
                    final double y1 = Math.max(0, style.getPaddingTop());
                    final double y2 = image.getHeight() - style.getPaddingBottom();
        
                    final var line = new Line2D.Double(x, y1, x, y2);
                    graphics.draw(line);
                }
                break;
            case HORIZONTAL:
                for (int i = 0; i < image.getHeight(); i += style.getGridSize()) {
                    final double x1 = Math.max(0, style.getPaddingLeft());
                    final double x2 = image.getWidth() - style.getPaddingRight();
                    
                    final double y = Math.min(
                            Math.max(i, style.getPaddingTop()),
                            image.getHeight() - style.getPaddingBottom()
                    );
                    
                    final var line = new Line2D.Double(x1, y, x2, y);
                    graphics.draw(line);
                }
                break;
        }
        
        graphics.dispose();
    }
    
    public void drawSquares() {
        drawLines(Direction.VERTICAL);
        drawLines(Direction.HORIZONTAL);
    }
    
    public void drawHexes(Direction direction) {
        final Graphics2D graphics = image.createGraphics();
        graphics.setColor(style.getGridColour());
        graphics.setStroke(new BasicStroke(style.getLineWidth()));
        
        // Clip to padding box before drawing.
        graphics.clipRect(
                style.getPaddingLeft(),
                style.getPaddingTop(),
                image.getWidth() - style.getPaddingLeft() - style.getPaddingRight(),
                image.getHeight() - style.getPaddingTop() - style.getPaddingBottom()
        );
        
        // Calculate vertexes for a basic hexagon.
        final double apothem = style.getGridSize()/2.0;
        final double side = 2*((apothem*Math.sqrt(3))/3);
        final int[] xPoints = {
                0,
                (int)side,
                (int)(side + apothem * 0.6),
                (int)side,
                0,
                (int)(-(apothem * 0.6))
        };
        final int[] yPoints = {
                0,
                0,
                (int)apothem,
                (int)(2 * apothem),
                (int)(2 * apothem),
                (int)apothem
        };
        final int nPoints = 6;
        
        switch (direction) {
            case VERTICAL:
                for (
                        int y = style.getPaddingTop();
                        y < image.getHeight() - style.getPaddingBottom();
                        y += apothem
                ) {
                    for (
                            // Add apothem to ensure the leftmost edge doesn't exceed padding.
                            int x = style.getPaddingLeft() + (int)(apothem*0.6);
                            x < image.getWidth() - style.getPaddingRight();
                            x += side*3 // Allow room for alternating rows.
                    ) {
                        // Make a hex to position and draw.
                        var hex = new Polygon(xPoints, yPoints, nPoints);
                        int odd_row = (int)(side * 1.5 * ((y / apothem) % 2));
                        
                        hex.translate(x + odd_row, y);
                        graphics.draw(hex);
                    }
                }
                break;
            case HORIZONTAL:
                // Invert x and y to flip hexes.
                for (
                        int y = style.getPaddingTop();
                        y < image.getWidth() - style.getPaddingRight();
                        y += apothem
                ) {
                    for (
                            int x = style.getPaddingLeft() + (int)(apothem*0.6);
                            x < image.getHeight() - style.getPaddingBottom();
                            x += side*3 // Allow room for alternating rows.
                    ) {
                        var hex = new Polygon(yPoints, xPoints, nPoints);
                        int odd_row = (int)(side * 1.5 * ((y / apothem) % 2));
    
                        //noinspection SuspiciousNameCombination
                        hex.translate(y, x + odd_row);
                        graphics.draw(hex);
                    }
                }
                break;
        }

        graphics.dispose();
    }
}
