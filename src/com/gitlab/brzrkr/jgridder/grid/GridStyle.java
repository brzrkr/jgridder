package com.gitlab.brzrkr.jgridder.grid;


import java.awt.*;


public class GridStyle {
    private int paddingTop;
    private int paddingRight;
    private int paddingBottom;
    private int paddingLeft;
    private int gridSize;
    private int lineWidth;
    private Color gridColour;
    
    public GridStyle(
            final int paddingTop,
            final int paddingRight,
            final int paddingBottom,
            final int paddingLeft,
            final int gridSize,
            final int lineWidth,
            final javafx.scene.paint.Color gridColour
    ) {
        setPaddingTop(paddingTop);
        setPaddingRight(paddingRight);
        setPaddingBottom(paddingBottom);
        setPaddingLeft(paddingLeft);
        setGridSize(gridSize);
        setLineWidth(lineWidth);
        
        // Convert Color class.
        Color converted = ColourConverter.javaFXToAWT(gridColour);
        setGridColour(converted);
    }
    
    public GridStyle(final int gridSize, final int lineWidth) {
        this(
                0,
                0,
                0,
                0,
                gridSize,
                lineWidth,
                new javafx.scene.paint.Color(0.0, 0.0, 0.0, 1.0)
        );
    }
    
    /** Produces an empty (invisible) grid. */
    GridStyle() { this(0, 0); }
    
    public int getPaddingTop() { return paddingTop; }
    
    public void setPaddingTop(final int paddingTop) { this.paddingTop = paddingTop; }
    
    public int getPaddingRight() { return paddingRight; }
    
    public void setPaddingRight(final int paddingRight) { this.paddingRight = paddingRight; }
    
    public int getPaddingBottom() { return paddingBottom; }
    
    public void setPaddingBottom(final int paddingBottom) { this.paddingBottom = paddingBottom; }
    
    public int getPaddingLeft() { return paddingLeft; }
    
    public void setPaddingLeft(final int paddingLeft) { this.paddingLeft = paddingLeft; }
    
    public int getGridSize() { return gridSize; }
    
    public void setGridSize(final int gridSize) { this.gridSize = gridSize; }
    
    public Color getGridColour() { return gridColour; }
    
    public void setGridColour(final Color gridColour) { this.gridColour = gridColour; }
    
    public int getLineWidth() { return lineWidth; }
    
    public void setLineWidth(final int lineWidth) { this.lineWidth = lineWidth; }
}
